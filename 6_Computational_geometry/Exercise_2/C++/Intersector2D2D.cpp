#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D2D::NoIntersection;
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  _planeNormal1 = planeNormal;
  _planeTranslation1 = planeTranslation;
  return;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  _planeNormal2 = planeNormal;
  _planeTranslation2 = planeTranslation;
  return ;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
  bool intersection = false;
  // N3 = N1 ^ N2
  _planeNormal3(0) = _planeNormal1(1)*_planeNormal2(2) - _planeNormal1(2)*_planeNormal2(1);
  _planeNormal3(1) = _planeNormal1(2)*_planeNormal2(0) - _planeNormal1(0)*_planeNormal2(2);
  _planeNormal3(2) = _planeNormal1(0)*_planeNormal2(1) - _planeNormal1(1)*_planeNormal2(0);

  int flag = 0;
  for (int i=0; i<3; i++) //controllo se tutte le componenti di N3 sono nulle (minori della tolleranza)
  {
      if (abs(_planeNormal3(i)) < toleranceParallelism)
          flag ++;
  }
  if (flag==3) //se N3 è nullo
  {
    if (abs(_planeTranslation1 - _planeTranslation2) < toleranceParallelism) // se abs(d1 - d2)<toll sono coincidenti altrimenti paralleli
        intersectionType = Coplanar;
    else
        intersectionType = NoIntersection;
  }
  else //se N3 non è nullo
  {
     intersection = true;
     intersectionType = LineIntersection;
  }
  return intersection;
}
