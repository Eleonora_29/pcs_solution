#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoIntersection;
    intersectionParametricCoordinate = 0;
}
Intersector2D1D::~Intersector2D1D()
{

}

// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  _planeNormal = planeNormal;
  _planeTranslation = planeTranslation;
  return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
  _lineOrigin = lineOrigin;
  _lineTangent = lineTangent;
  return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
  intersectionType = NoIntersection;
  bool intersection = false;

  // s = (d - N*x0) / N*t

  check = 0;
  for (int i=0; i<3; i++)
    check += _planeNormal(i)* _lineTangent(i); // check = N*t
  if (abs(check) > toleranceIntersection) // si intersecano se N*t != 0 (denominatore)
  {
     intersectionType = PointIntersection;
     intersection = true;
  }
  c=0;
  for (int i=0; i<3; i++)
    c += _planeNormal(i)* _lineOrigin(i); // c = N*x0
  if (abs(check) < toleranceParallelism)
  {
     if (_planeTranslation == c) // se d - N*x0 = 0 sono coplanari
        intersectionType = Coplanar;
     else
        intersectionType = NoIntersection;
  }
  return intersection;
 }

Vector3d Intersector2D1D::IntersectionPoint()
{
    intersectionParametricCoordinate = (_planeTranslation - c) / check; // s = (d - N*x0) / N*t
    Vector3d intersection;
    for (int i=0; i<3; i++)
      intersection(i) = _lineOrigin(i) +  intersectionParametricCoordinate * _lineTangent(i); // x= x0 + s*t
    return intersection;
}
