#ifndef __TEST_SHOPPING_H
#define __TEST_SHOPPING_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "IShoppingApp.h"

using namespace ShoppingLibrary;

using namespace testing;

namespace ShoppingTesting {

#define GTEST_COUT std::cerr << "[          ] [ INFO ] "

  unsigned int numElements = 100000;

  string RandomString(const int len)
  {
    static const char alphanum[] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    string s;
    s.resize(len + 1);
    for (int i = 0; i < len; ++i) {
      s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
    s[len] = 0;
    return s;
  }

  void FillList(IShoppingApp& application, const unsigned int& numberElements)
  {
    for (unsigned int s = 0; s < numberElements; s++)
      application.AddElement(RandomString(10));
  }

  TEST(TestShopping, TestVector)
  {
    VectorShoppingApp application;

    try
    {
      clock_t t;
      t = clock();
      FillList(application, numElements);
      t = clock() - t; //misura il tempo che ci mette a fare l'azione
      GTEST_COUT << "Insert: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl; //per ottenere il tempo divide quello che ha ottenuto per la velocità con cui il computer fa le operazioni
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      clock_t t;
      t = clock();
      application.Undo();
      t = clock() - t;
      GTEST_COUT << "Undo: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      application.Reset();
      FillList(application, numElements);
      clock_t t;
      t = clock();
      application.SearchElement("sadaskjbdk");
      t = clock() - t;
      GTEST_COUT << "Search: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }
  }

  TEST(TestShopping, TestList)
  {
    ListShoppingApp application;

    try
    {
      clock_t t;
      t = clock();
      FillList(application, numElements);
      t = clock() - t;
      GTEST_COUT << "Insert: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      clock_t t;
      t = clock();
      application.Undo();
      t = clock() - t;
      GTEST_COUT << "Undo: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      application.Reset();
      FillList(application, numElements);
      clock_t t;
      t = clock();
      application.SearchElement("sadaskjbdk");
      t = clock() - t;
      GTEST_COUT << "Search: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }
  }

  TEST(TestShopping, TestStack)
  {
    StackShoppingApp application;

    try
    {
      clock_t t;
      t = clock();
      FillList(application, numElements);
      t = clock() - t;
      GTEST_COUT << "Insert: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      clock_t t;
      t = clock();
      application.Undo();
      t = clock() - t;
      GTEST_COUT << "Undo: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      application.Reset();
      FillList(application, numElements);
      clock_t t;
      t = clock();
      application.SearchElement("sadaskjbdk");
      t = clock() - t;
      GTEST_COUT << "Search: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }
  }

  TEST(TestShopping, TestQueue)
  {
    QueueShoppingApp application;

    try
    {
      clock_t t;
      t = clock();
      FillList(application, numElements);
      t = clock() - t;
      GTEST_COUT << "Insert: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      clock_t t;
      t = clock();
      application.Undo();
      t = clock() - t;
      GTEST_COUT << "Undo: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      application.Reset();
      FillList(application, numElements);
      clock_t t;
      t = clock();
      application.SearchElement("sadaskjbdk");
      t = clock() - t;
      GTEST_COUT << "Search: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }
  }

  TEST(TestShopping, TestHash)
  {
    HashShoppingApp application;

    try
    {
      clock_t t;
      t = clock();
      FillList(application, numElements);
      t = clock() - t;
      GTEST_COUT << "Insert: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      clock_t t;
      t = clock();
      application.Undo();
      t = clock() - t;
      GTEST_COUT << "Undo: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }

    try
    {
      application.Reset();
      FillList(application, numElements);
      clock_t t;
      t = clock();
      application.SearchElement("sadaskjbdk");
      t = clock() - t;
      GTEST_COUT << "Search: "<< t<< " clicks ("<< ((float)t) / CLOCKS_PER_SEC<< " seconds)" << endl;
    }
    catch (const exception& exception) {
      FAIL();
    }
  }
}

#endif // __TEST_SHOPPING_H
