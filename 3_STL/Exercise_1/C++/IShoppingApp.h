#ifndef SHOPPING_H
#define SHOPPING_H

#include <iostream>
#include <vector>
#include <list>
#include <stack>
#include <queue>
#include <unordered_set> //hash table

using namespace std;

namespace ShoppingLibrary {

  class IShoppingApp {
    public:
      virtual unsigned int NumberElements() const = 0;
      virtual void AddElement(const string& product) = 0;
      virtual void Undo() = 0;
      virtual void Reset() = 0;
      virtual bool SearchElement(const string& product) = 0;
  };

  class VectorShoppingApp : public IShoppingApp {
    private:
      vector <string> elements; //vector prende in imput il tipo che contiene tra i template
    public:
      void Temp(){
      //    elements.resize(10); // se so la dimensione finale del vettore
      //    elements[5]='pippo';

          elements.reserve(10); //riservo nella memoria almeno 10 elementi, poi il vettore si ridimensionerà di volta in volta
          elements.push_back("pippo2"); //aggiungo in coda, senza sapere la dimensione del vettore
      }
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); } //toglie l'ultimo elemento
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
      {
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i< numElements; i++)
          {
              if (elements[i] ==product)
                  return true;
          }
          return false;
      }
  };

  class ListShoppingApp : public IShoppingApp {
    private:
      list<string> elements;

    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push_back(product); }
      void Undo() { elements.pop_back(); }
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product)
      {
        for (list<string>::iterator it= elements.begin(); it != elements.end(); it++) //l'iteratore è un puntatore serve per scorrere nella lista
        {
            const string& element = *it; //perché è un puntatore
            if (element == product)
                return true;
        }
        return false;
      }
  };

  // le code sono FIFO
  class QueueShoppingApp : public IShoppingApp {
    private:
      queue<string> elements;
    public:
      unsigned int NumberElements() const { elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() {
          queue<string> queueNew;
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i<numElements - 1; i++) //li devo scodare tutti tranne l'ultimo
          {
              queueNew.push(elements.back());
              elements.pop();
          }
          elements.pop(); //elimino l'ultimo
          for (unsigned int i=0; i<numElements - 1; i++)
          {
              elements.push(queueNew.front());
              queueNew.pop();
          }
      }
      void Reset() {
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i<numElements; i++)
          {
              elements.pop();
          }
      }
      bool SearchElement(const string& product) {
          queue<string> queueNew;
          bool found= false;
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i<numElements; i++)
          {
             const string& element = elements.back();
             if (element==product)
             {
                 return true;
                 break;
             }
             queueNew.push(element);
             elements.pop();
          }
          unsigned int numElementsNew =queueNew.size();
          for (unsigned int i=0; i<numElementsNew; i++)
          {
              AddElement(queueNew.front());
              queueNew.pop();
          }
          return found;
      }
  };

  //le pile sono LIFO
  class StackShoppingApp : public IShoppingApp {
    private:
      stack<string> elements;
    public:
      unsigned int NumberElements() const { return elements.size(); }
      void AddElement(const string& product) { elements.push(product); }
      void Undo() { elements.pop(); }
      void Reset() {
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i<numElements; i++)
              elements.pop(); //nella pila devo svuotare un elemento per volta
      }
      bool SearchElement(const string& product) {
          stack<string> stackNew; //pila accessoria
          bool found = false; //per tenere conto se ho trovato il mio elemento
          unsigned int numElements = NumberElements();
          for (unsigned int i=0; i<numElements; i++)
          {
             string& element = elements.top();
             if (element==product)
             {
                 found=true;
                 break;
             }
             stackNew.push(element); //metto l'elemento che non mi serve nella pila accessoria
             elements.pop(); //lo tolgo da quella originale e continuo la ricerca
          }
          unsigned int numElementsNew = NumberElements(); //adesso devo rimettere a posto gli elementi nella pila originale
          for (unsigned int i=0; i<numElements; i++)
          {
              elements.push(stackNew.top());
              stackNew.pop();
          }
          return found;
      }
  };

  class HashShoppingApp : public IShoppingApp {
    private:
      unordered_set<string> elements;
      string lastElement;
    public:
      unsigned int NumberElements() const {elements.size(); }
      void AddElement(const string& product) {elements.insert(product); }
      void Undo() { elements.erase(lastElement);}
      void Reset() { elements.clear(); }
      bool SearchElement(const string& product) { return elements.find(product) != elements.end(); }
  };
}

#endif // SHOPPING_H
