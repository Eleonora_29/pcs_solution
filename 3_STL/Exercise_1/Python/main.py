class IShoppingApp:
    def numberElements(self) -> int:
        return 0

    def addElement(self, product: str):
        pass

    def undo(self):
        pass

    def reset(self):
        pass

    def searchElement(self, product: str) -> bool:
        return False


class VectorShoppingApp(IShoppingApp):
    def __init__(self):
        # self.elements = [0]*100 è come resize(100)
        self.elements = []

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.append(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        numElements: int = self.numberElements()
        for i in range(0, numElements):
            if self.elements[i] is product:
                return True
        return False


class ListShoppingApp(IShoppingApp):
    def __init__(self):
        self.elements = []

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.append(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        for element in self.elements: #per ciascun elemento all'interno della mia lista
            if element is product:
                return True
        return False


class StackShoppingApp(IShoppingApp):
    def __init__(self):
        self.elements = []

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.append(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        stackUndo = [] #pila di supporto
        found: bool = False
        numElements = self.numberElements()
        for i in range (0, numElements):
            element = self.elements.pop()
            stackUndo.append(element) #prima tolgo l'elemento e poi lo leggo, se no lo perdo
            if (element is product):
                found = True
                break
            numElementsUndo = len(stackUndo)
            for i in range (0, numElementsUndo):
                self.elements.append(stackUndo.pop())
            return found


class QueueShoppingApp(IShoppingApp):
    def __init__(self):
        self.elements = []

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.append(product)

    def undo(self):
        queueUndo= []
        numElements = self.numberElements()
        for i in range(0, numElements-1):
            queueUndo.append(self.elements.pop())
        self.elements.pop()
        numElements = self.numberElements()
        for i in range(0, numElements-1):
            self.elements.append(queueUndo.pop())

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        queueUndo = []
        found: bool = False
        numElements = self.numberElements()
        for i in range(0, numElements):
            element = self.elements.pop()
            queueUndo.append(element)
            if (element is product):
                found = True
                break
            numElementsUndo = len(queueUndo)
            for i in range(0, numElementsUndo):
                self.elements.append(queueUndo.pop())
            return found


class HashShoppingApp(IShoppingApp):
    def __init__(self):
        self.elements = set()

    def numberElements(self) -> int:
        return len(self.elements)

    def addElement(self, product: str):
        self.elements.add(product)

    def undo(self):
        self.elements.pop()

    def reset(self):
        self.elements.clear()

    def searchElement(self, product: str) -> bool:
        return product in self.elements #se il prodotto è nella hash ritorna vero
