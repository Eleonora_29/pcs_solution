class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name
        self.ingredientiPizza = []

    def addIngredient(self, ingredient: Ingredient):
        self.ingredientiPizza.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.ingredientiPizza)

    def computePrice(self) -> int:
        tot=0
        for i in range (0, self.numIngredients()):
            tot=tot + self.ingredientiPizza[i].Price #sommo il prezzo di ciascun ingrediente della pizza
        return tot


class Order:
    def __init__(self):
        self.pizzeOrdine = []

    def numPizzas(self) -> int:
        return len(self.pizzeOrdine)

    def addPizza(self, pizza: Pizza):
        self.pizzeOrdine.append(pizza)

    def getPizza(self, position: int) -> Pizza:
        if (position > 0 and position < (self.numPizzas()+1)): #controllo che la posizione sia nel range delle pizze dell'ordine
            return self.pizzeOrdine[position-1]
        else:
            raise ValueError("Position passed is wrong")

    def initializeOrder(self, numPizzas: int):
        self.pizzeOrdine=[0]*numPizzas

    def computeTotal(self) -> int:
        tot=0
        for pizza in self.pizzeOrdine:
            tot += pizza.computePrice() #per ciascuna pizza dell'ordine calcolo il prezzo
        return tot


class Pizzeria:
    def __init__(self):
        self.ingredientiOrdinati = []
        self.ingredientiCucina = []
        self.pizzeMenu = []
        self.ordini = []

    def addIngredient(self, name: str, description: str, price: int):
        for nuovoIng in self.ingredientiCucina: #scorro gli ingredienti della cucina
            if nuovoIng.Name == name: #se quello che voglio inserire esiste già
                raise ValueError("Ingredient already inserted")
        nuovoIng= Ingredient(name, price, description) #gli passo i parametri presi in imput dalla funzione
        self.ingredientiCucina.append(nuovoIng)

    def findIngredient(self, name: str) -> Ingredient:
        for ing in self.ingredientiCucina:
            if ing.Name == name:
                return ing
        raise ValueError("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        for nuovaPizza in self.pizzeMenu:
            if nuovaPizza.Name == name:
                raise ValueError("Pizza already inserted")
        nuovaPizza = Pizza(name)
        for i in ingredients: #scorro gli ingredienti della nuova pizza
            for j in self.ingredientiCucina: #scorro gli ingredienti della cucina per cercare quelli contenuti nella nuova pizza e inserirli nella nuova pizza
                if (i == j.Name):
                    nuovaPizza.ingredientiPizza.append(j)
        self.pizzeMenu.append(nuovaPizza)

    def findPizza(self, name: str) -> Pizza:
        for piz in self.pizzeMenu:
            if piz.Name == name:
                print(piz.Name)
                return piz
        raise ValueError("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if (len(pizzas) == 0):
            raise ValueError("Empty order")
        nuovoOrdine = Order()
        for i in range (0, len(pizzas)):
            for piz in self.pizzeMenu:
                if pizzas[i] == piz.Name: #quando trovo nel menù la pizza passata dalla funzione, la aggiungo all'ordine
                    nuovoOrdine.pizzeOrdine.append(piz)
        nuovoOrdine.numOrdine = 1000 + len(self.ordini) #i numeri degli ordini partono da 1000
        self.ordini.append(nuovoOrdine)
        return nuovoOrdine.numOrdine

    def findOrder(self, numOrder: int) -> Order:
        for ord in self.ordini:
            if ord.numOrdine == numOrder:
                return ord
        raise ValueError("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        for ord in self.ordini: #scorro la lista degli ordini
            if (ord.numOrdine == numOrder):
                receipt = ''
                for piz in ord.pizzeOrdine:
                    receipt += "- " + piz.Name + ", " +str(piz.computePrice())+ " euro" + "\n" #tipo - Margherita, 5 euro
                receipt += "  TOTAL: " + str(ord.computeTotal()) + " euro" + "\n"
                return receipt
        raise ValueError("Order not found")

    def listIngredients(self) -> str:
        lista = ''
        temp = []
        for ing in self.ingredientiCucina:
            temp.append(ing.Name)
        temp.sort()
        for t in temp:
            for ing in self.ingredientiCucina:
                if t == ing.Name:
                    self.ingredientiOrdinati.append(ing)
        for i in range (0, len(self.ingredientiOrdinati)): #tipo Mozzarella - 'Traditionally southern Italian cheese': 3 euro
            lista += self.ingredientiOrdinati[i].Name + " - '" + self.ingredientiOrdinati[i].Description + "': " + str(self.ingredientiOrdinati[i].Price) + " euro" + "\n"
        return lista

    def menu(self) -> str:
        ilmenu = ''
        for i in self.pizzeMenu: #tipo Margherita (2 ingredients): 5 euro
            ilmenu += i.Name + " (" + str(i.numIngredients()) + " ingredients): " + str(i.computePrice()) + " euro" + "\n"
        return ilmenu