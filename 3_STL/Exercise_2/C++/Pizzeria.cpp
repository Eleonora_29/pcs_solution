#include "Pizzeria.h"

namespace PizzeriaLibrary {

void Pizza::AddIngredient(const Ingredient &ingredient)
{
    ingredientiPizza.push_back(ingredient);
}

int Pizza::NumIngredients() const { return ingredientiPizza.size(); }

int Pizza::ComputePrice() const
{
    int tot=0;
    for (int i=0; i<NumIngredients(); i++)
    {
        tot = tot + ingredientiPizza[i].Price; //sommo il prezzo di ciascun ingrediente della pizza
    }
    return tot;
}

void Order::InitializeOrder(int numPizzas) { pizzeOrdine.reserve(numPizzas); }

void Order::AddPizza(const Pizza &pizza) { pizzeOrdine.push_back(new Pizza(pizza)); }

int Order::NumPizzas() const { return pizzeOrdine.size(); }

const Pizza &Order::GetPizza(const int &position) const
{
    if (position>0 && position<(NumPizzas()+1)) //controllo che la posizione sia nel range delle pizze dell'ordine
        return *pizzeOrdine[position-1];
    else
        throw runtime_error("Position passed is wrong");
}

int Order::ComputeTotal() const
{
    int tot=0;
    for (int i=0; i<NumPizzas(); i++)
    {
        tot = tot + pizzeOrdine[i]->ComputePrice(); //per ciascuna pizza dell'ordine calcolo il prezzo
    }
    return tot;
}

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price)
{
    Ingredient nuovoIngrediente;
    for (unsigned int i=0; i<ingredientiCucina.size(); i++)
    {
        if (ingredientiCucina[i].Name == name) //se l'ingrediente esiste già
            throw runtime_error("Ingredient already inserted");
    }
    nuovoIngrediente.Name= name;
    nuovoIngrediente.Description=description;
    nuovoIngrediente.Price= price;
    ingredientiCucina.push_back(nuovoIngrediente);
    std::sort(ingredientiCucina.begin(), ingredientiCucina.end()); //ordino gli ingredienti in ordine alfabetico
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    for (unsigned int i=0; i<ingredientiCucina.size(); i++)
    {
        if (ingredientiCucina[i].Name == name)
            return ingredientiCucina[i];
    }
    throw runtime_error("Ingredient not found");
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    for (unsigned int i=0; i<pizzeMenu.size(); i++)
    {
        if (pizzeMenu[i].Name == name)
            throw runtime_error("Pizza already inserted");
    }
    Pizza nuovaPizza;
    nuovaPizza.Name = name;
    for (unsigned int i=0; i<ingredients.size(); i++) //scorro gli ingredienti della nuova pizza
    {
        for (unsigned int j=0; j<ingredientiCucina.size(); j++) //scorro gli ingredienti della cucina per cercare quelli contenuti nella nuova pizza e inserirli nella nuova pizza
        {
            if (ingredients[i]==ingredientiCucina[j].Name) {
                nuovaPizza.ingredientiPizza.push_back(ingredientiCucina[j]);
            }
        }
    }
    pizzeMenu.push_back(nuovaPizza);
}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    for (unsigned int i=0; i<pizzeMenu.size(); i++)
    {
        if (pizzeMenu[i].Name == name)
            return pizzeMenu[i];
    }
    throw runtime_error("Pizza not found");
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    if (pizzas.size()==0)
        throw runtime_error("Empty order");
    Order nuovoOrdine;
    for (unsigned int i=0; i<pizzas.size(); i++)
    {
        for (unsigned int j=0; j<pizzeMenu.size(); j++)
        {
            if (pizzas[i]==pizzeMenu[j].Name) //quando trovo nel menù la pizza passata dalla funzione, la aggiungo all'ordine
                nuovoOrdine.pizzeOrdine.push_back(new Pizza(pizzeMenu[j]));
        }
    }
    nuovoOrdine.numOrdine = 1000 + ordini.size(); //i numeri degli ordini partono da 1000
    ordini.push_back(nuovoOrdine);
    return nuovoOrdine.numOrdine;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    for (unsigned int i=0; i<ordini.size(); i++)
    {
        if (ordini[i].numOrdine == numOrder) {
            return ordini[i];
        }
    }
    throw runtime_error("Order not found");
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    string receipt;
    for (unsigned int i=0; i<ordini.size(); i++) //scorro la lista degli ordini
    {
        if (ordini[i].numOrdine == numOrder)
        {
            for (int j=0; j<ordini[i].NumPizzas(); j++) //per ogni pizza nell'ordine scrivo una riga di scontrino tipo - Margherita, 5 euro
            {
                receipt = receipt + "- " + ordini[i].pizzeOrdine[j]->Name + ", " + to_string(ordini[i].pizzeOrdine[j]->ComputePrice())+ " euro" + "\n";
            }
            receipt = receipt +  "  TOTAL: " + to_string(ordini[i].ComputeTotal()) + " euro" + "\n";    //aggiungo allo scontrino il totale
            return receipt;
        }
    }
    throw runtime_error("Order not found");
}

string Pizzeria::ListIngredients() const
{
    string list;
    for (unsigned int i=0; i<ingredientiCucina.size(); i++) //per ogni ingrediente scrivo tipo Mozzarella - 'Traditionally southern Italian cheese': 3 euro
    {
        list = list + ingredientiCucina[i].Name + " - '" + ingredientiCucina[i].Description + "': " + to_string(ingredientiCucina[i].Price) + " euro" + "\n";
    }
    return list;

}

string Pizzeria::Menu() const
{
     string menu;
     for (int i=(pizzeMenu.size()-1); i>-1; i--) //ritorno le pizze in ordine inverso di inserimento tipo Margherita (2 ingredients): 5 euro
     {
         menu = menu + pizzeMenu[i].Name + " (" + to_string(pizzeMenu[i].NumIngredients()) + " ingredients): " + to_string(pizzeMenu[i].ComputePrice()) + " euro" + "\n";
     }
     return menu;
}



}
