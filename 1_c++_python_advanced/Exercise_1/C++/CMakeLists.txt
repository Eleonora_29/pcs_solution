cmake_minimum_required(VERSION 3.5)

project(linearSystem LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(gedim_include
  ${CMAKE_CURRENT_SOURCE_DIR}/eigen/include
  )

include_directories(SYSTEM ${gedim_include})
add_executable(linearSystem main.cpp)
