#ifndef GOOGLEMAP_H
#define GOOGLEMAP_H

#include <iostream>
#include <vector>

#include "bus.h"

using namespace std;
using namespace BusLibrary;

namespace GoogleMapLibrary {

  class BusStation : public IBusStation {
  private:
      string _busFilePath;
      int _numberBuses;
      vector<Bus> _buses;

      void Reset();

    public:
      BusStation(const string& busFilePath) { _busFilePath = busFilePath; } //qui viene chiamato il costruttore copia
      void Load();
      int NumberBuses() const { return _numberBuses;; }
      const Bus& GetBus(const int& idBus) const;
  };

  class MapData : public IMapData {

    private:
      string _mapFilePath;
      int _numberBusStops;
      int _numberStreets;
      int _numberRoutes;
      vector<BusStop> _busStops;
      vector<Street> _streets;
      vector<Route> _routes;
      vector<int> _streetFrom;
      vector<int> _streetTo;
      vector<vector<int>> _routesStreets;

      void Reset();

    public:
      MapData(const string& mapFilePath) { _mapFilePath = mapFilePath; }
      void Load();
      int NumberRoutes() const { return _numberRoutes; }
      int NumberStreets() const { return _numberStreets;; }
      int NumberBusStops() const { return _numberBusStops; }
      const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const;
      const Route& GetRoute(const int& idRoute) const;
      const Street& GetStreet(const int& idStreet) const;
      const BusStop& GetStreetFrom(const int& idStreet) const;
      const BusStop& GetStreetTo(const int& idStreet) const;
      const BusStop& GetBusStop(const int& idBusStop) const;
  };

  class RoutePlanner : public IRoutePlanner {
    public:
      static int BusEnhancementStopTime;

    private:
      const IMapData& _mapData;
      const IBusStation& _busStation;

    public:
      RoutePlanner(const IMapData& mapData,
                   const IBusStation& busStation) :
      _mapData(mapData),
      _busStation(busStation) { }

      int ComputeRouteTravelTime(const int& idRoute) const;
      int ComputeRouteCost(const int& idBus, const int& idRoute) const { throw runtime_error("No cost computation available"); }
  };

  class MapViewer : public IMapViewer {
    private:
      const IMapData& _mapData;

    public:
      MapViewer(const IMapData& mapData)  : _mapData(mapData) { }
      string ViewRoute(const int& idRoute) const;
      string ViewStreet(const int& idStreet) const;
      string ViewBusStop(const int& idBusStop) const;
  };
}

#endif // GOOGLEMAP_H
