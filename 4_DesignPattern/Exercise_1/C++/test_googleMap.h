#ifndef __TEST_GOOGLEMAP_H
#define __TEST_GOOGLEMAP_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "googleMaps.h"

using namespace testing;

namespace GoogleMapsTest {

  TEST(TestGoogleMap, BusStation)
  {
    try
    {
      GoogleMapLibrary::BusStation station("fileNotExisting.txt"); //passa un file che non esiste e si aspetta un'eccezione
      station.Load();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Something goes wrong"));
    }

    GoogleMapLibrary::BusStation station("googleMapBuses.txt"); //file che esiste

    try
    {
      station.Load();
      EXPECT_EQ(station.NumberBuses(), 3);
      EXPECT_EQ(station.GetBus(1).Id, 1);
      EXPECT_EQ(station.GetBus(2).FuelCost, 0);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      station.GetBus(4);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Bus 4 does not exists"));
    }
  }

  TEST(TestGoogleMap, MapData)
  {
    try
    {
      GoogleMapLibrary::MapData map("fileNotExisting.txt");
      map.Load();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Something goes wrong"));
    }

    GoogleMapLibrary::MapData map("googleMap.txt");

    try
    {
      map.Load();
      EXPECT_EQ(map.NumberBusStops(), 3);
      EXPECT_EQ(map.NumberStreets(), 4);
      EXPECT_EQ(map.NumberRoutes(), 2);
      EXPECT_EQ(map.GetBusStop(1).Id, 1);
      EXPECT_EQ(map.GetBusStop(2).Name, "Politecnico");
      EXPECT_EQ(map.GetBusStop(3).Latitude, 450771);
      EXPECT_EQ(map.GetBusStop(3).Longitude, 76781);
      EXPECT_EQ(map.GetStreet(1).Id, 1);
      EXPECT_EQ(map.GetStreet(2).TravelTime, 48);
      EXPECT_EQ(map.GetStreetFrom(3).Id, 1);
      EXPECT_EQ(map.GetStreetTo(3).Id, 3);
      EXPECT_EQ(map.GetRoute(1).Id, 1);
      EXPECT_EQ(map.GetRoute(2).NumberStreets, 2);
      EXPECT_EQ(map.GetRouteStreet(2, 1).Id, 4);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      map.GetBusStop(4);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("BusStop 4 does not exists"));
    }

    try
    {
      map.GetStreet(5);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Street 5 does not exists"));
    }

    try
    {
      map.GetRoute(4);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Route 4 does not exists"));
    }

    try
    {
      map.GetStreetFrom(5);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Street 5 does not exists"));
    }

    try
    {
      map.GetStreetTo(5);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Street 5 does not exists"));
    }

    try
    {
      map.GetRouteStreet(4, 3);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Route 4 does not exists"));
    }

    try
    {
      map.GetRouteStreet(2, 18);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("Street at position 18 does not exists"));
    }
  }

  class MockMapData : public IMapData {
  //per ciascun metodo della mia interfaccia, nella classe mock ci deve essere una classe di google (macro) che va a imitare quel metodo
  //macro di google: si dividono in costanti e non costanti(con tot parametri in imput)
    public:
      MOCK_METHOD0(Load, void()); //METHOD0 cioè 0 imput
      MOCK_CONST_METHOD0(NumberRoutes, int());
      MOCK_CONST_METHOD0(NumberStreets, int());
      MOCK_CONST_METHOD0(NumberBusStops, int());
      MOCK_CONST_METHOD1(GetRoute, const Route&(const int& idRoute)); //1 imput
      MOCK_CONST_METHOD1(GetStreet, const Street&(const int& idStreet));
      MOCK_CONST_METHOD1(GetBusStop, const BusStop&(const int& idBusStop));
      MOCK_CONST_METHOD1(GetStreetFrom, const BusStop&(const int& idStreet));
      MOCK_CONST_METHOD1(GetStreetTo, const BusStop&(const int& idStreet));
      MOCK_CONST_METHOD2(GetRouteStreet, const Street&(const int& idRoute, const int& streetPosition));
  };

  class MockBusStation : public IBusStation {
    public:
      MOCK_METHOD0(Load, void());
      MOCK_CONST_METHOD1(GetBus, const Bus&(const int& idBus));
      MOCK_CONST_METHOD0(NumberBuses, int());
  };

  //le classi mock mi permettono di restituire ciò che voglio io, di metodi che non sempre ho sotto controllo, per verificare il funzionamento dell'implementazione

  TEST(TestGoogleMap, RoutePlanner) {
    MockMapData mapData; //map data: oggetto della classe mock che ho creato
    MockBusStation busStation;
    GoogleMapLibrary::RoutePlanner routePlanner(mapData, busStation); //creo gli oggetti da cui dipende attraverso funzioni stupide: mock, che non implementano nessuna logica

    Route route{1, 3};
    Street street1{1, 2}, street2{2, 4}, street3{3, 7}; //creo le referenze che voglio che le funzioni mock restituiscano

    EXPECT_CALL(mapData, GetRoute(1)).WillRepeatedly(ReturnRef(route));
    EXPECT_CALL(mapData, GetRouteStreet(1, 0)).WillOnce(ReturnRef(street1)); //ritorna una referenza a una strada che ho creato io
    EXPECT_CALL(mapData, GetRouteStreet(1, 1)).WillOnce(ReturnRef(street2));
    EXPECT_CALL(mapData, GetRouteStreet(1, 2)).WillOnce(ReturnRef(street3));
    // WillOnce vuol dire che restituisce quella referenza solo la prima volta che viene chiamato. Se lo richiamo si spacca. Invece WillRepeatedly restituisce sempre quella referenza
    try
    {
      routePlanner.BusEnhancementStopTime = 2;
      EXPECT_EQ(routePlanner.ComputeRouteTravelTime(1), 26);
    }
    catch (const exception& exception)
    {
      FAIL();
    }

    try
    {
      routePlanner.ComputeRouteCost(1, 1);
      FAIL();
    }
    catch (const exception& exception)
    {
      EXPECT_THAT(std::string(exception.what()), Eq("No cost computation available"));
    }
  }

  TEST(TestGoogleMap, MapViewer) {
    MockMapData mapData;
    GoogleMapLibrary::MapViewer mapViewer(mapData);

    Route route{1, 2};
    Street street1{1, 20}, street2{2, 40};
    BusStop start{1, 100000, 120000, "Start"}, middle{2, 200000, 210000, "Middle"}, end{3, 230000, 410000, "End"};

    EXPECT_CALL(mapData, GetRoute(1)).WillRepeatedly(ReturnRef(route));
    EXPECT_CALL(mapData, GetRouteStreet(1, 0)).WillRepeatedly(ReturnRef(street1));
    EXPECT_CALL(mapData, GetRouteStreet(1, 1)).WillRepeatedly(ReturnRef(street2));
    EXPECT_CALL(mapData, GetStreetFrom(1)).WillRepeatedly(ReturnRef(start));
    EXPECT_CALL(mapData, GetStreetTo(1)).WillRepeatedly(ReturnRef(middle));
    EXPECT_CALL(mapData, GetStreetFrom(2)).WillRepeatedly(ReturnRef(middle));
    EXPECT_CALL(mapData, GetStreetTo(2)).WillRepeatedly(ReturnRef(end));
    EXPECT_CALL(mapData, GetBusStop(1)).WillRepeatedly(ReturnRef(start));
    EXPECT_CALL(mapData, GetBusStop(2)).WillRepeatedly(ReturnRef(middle));
    EXPECT_CALL(mapData, GetBusStop(3)).WillRepeatedly(ReturnRef(end));
    try
    {
      EXPECT_EQ(mapViewer.ViewBusStop(1), "Start (10.000000, 12.000000)");
      EXPECT_EQ(mapViewer.ViewStreet(1), "1: Start -> Middle");
      EXPECT_EQ(mapViewer.ViewRoute(1), "1: Start -> Middle -> End");
    }
    catch (const exception& exception)
    {
      FAIL();
    }
  }
}
#endif // __TEST_GOOGLEMAP_H
