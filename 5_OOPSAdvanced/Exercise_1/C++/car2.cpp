#include "car2.h"

namespace CarLibrary2 {
  Car::Car(const string& _producer, const string& _model, const string& _color)
  {
    power = 0.0;
    producer = _producer;
    model = _model;
    color = _color;
  }

  Car Car::operator +(const Car& car)
  {
    Car tempCar(producer, model, color);
    tempCar.power = power + (car.power); //creo una nuova variabile che � la somma di altre due

    return tempCar;
  }

  Car& Car::operator =(const Car& car)
  {
    color = car.color;
    model = car.model;
    producer = car.producer;
    power = car.power;

    return *this;
  }

  Car& Car::operator +=(const Car& car)
  {
    color = car.color;
    model = car.model;
    producer = car.producer;
    power += car.power; //incremento la stessa variabile

    return *this; //ritorno lei stessa incrementata
  }

}
