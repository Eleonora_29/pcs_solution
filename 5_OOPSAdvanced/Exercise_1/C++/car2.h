#ifndef CAR2_H
#define CAR2_H

#include <iostream>
#include <sstream>
#include <tuple>

using namespace std;

namespace CarLibrary2 {

  class Car
  {
    public:
      double power;
      string producer;
      string model;
      string color;

    public:
      Car() { power = 0.0; producer = " "; model = " "; color = " ";}
      ~Car() { }
      Car(const Car& car) { *this = car; } //costruttore copia

      Car(const string& _producer,
          const string& _model,
          const string& _color);

      void SetPower(const double& _power) { power = _power;}

      string Show() const { ostringstream os;
                            os << model << " (" << producer << "): color " << color;
                            return os.str();  }

      Car& operator =(const Car& car);

      Car& operator +=(const Car& car);

      Car operator +(const Car& car);
      Car operator +(const double& _power) { Car tempCar(producer,model,color); tempCar.power = power + _power; return tempCar;}


      inline friend bool operator<(const Car& lhs, const Car& rhs)
      {
        return lhs.power < rhs.power; //ritorna vero se la potenza del primo oggetto � minore di quella del secondo
      }

      inline friend bool operator>(const Car& lhs, const Car& rhs)
      {
        return rhs.power < lhs.power;
      }

      friend ostream& operator<<(ostream& stream, const Car& car)
      {
        stream << car.Show() << endl;

        return stream;
      }
  };



  class ICarFactory
  {
    public:

      virtual Car* Create(const string& color, const string& model) = 0;
  };

  class Ford : public ICarFactory
  {
    public:
      Car* Create(const string& color, const string& model = "Mustang") { return new Car("Ford", model, color); }
  };

  class Toyota : public ICarFactory
  {
    public:
      Car* Create(const string& color, const string& model = "Prius") { return new Car("Toyota", model, color); }
  };

  class Volkswagen : public ICarFactory
  {
    public:
      Car* Create(const string& color, const string& model = "Golf") { return new Car("Volkswagen", model, color); }
  };
}

#endif // CAR2_H
