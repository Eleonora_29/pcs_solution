#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  Point::Point(const double &x, const double &y)
  {
    X = x;
    Y = y;
  }

  double Point::ComputeNorm2() const
  {
      const Point A;
      return sqrt(pow((A.X), 2)+pow((A.Y), 2));
  }

  double Point::Distance(const Point& A, const Point& B) //distanza tra i due punti
  {
      return sqrt(pow((A.X-B.X), 2)+pow((A.Y-B.Y), 2));
  }

  Point Point::operator+(const Point& point) const
  {
    Point tempPoint(X,Y);
    tempPoint.X = X + (point.X);
    tempPoint.Y = Y + (point.Y);
    return tempPoint;
  }

  Point Point::operator-(const Point& point) const
  {
    Point tempPoint(X,Y);
    tempPoint.X = X - (point.X);
    tempPoint.Y = Y - (point.Y);
    return tempPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
    X -= point.X;
    Y -= point.Y;
    return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
    X += point.X;
    Y += point.Y;
    return *this;
  }

  Ellipse::Ellipse(const Point &center, const double &a, const double &b)
  {
      _center = center;
      _a = a;
      _b = b;
  }

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*sqrt((pow((_a),2)+pow((_b),2))/2);
    }

  Circle::Circle(const Point &center, const double &radius) : Ellipse(center, radius, radius) {}

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      l1=Point::Distance(p2, p1);
      l2=Point::Distance(p2, p3);
      l3=Point::Distance(p1, p3);
  }

  void Triangle::AddVertex(const Point &point) //se non ha ancora tre vertici, ne aggiungo uno
  {
    if (points.size()<3)
        points.push_back(point);
    else
        throw runtime_error ("Il triangolo ha già tre vertici");
  }

  double Triangle::Perimeter() const
  {
      double perimeter = 0;
      perimeter = l1 + l2 + l3;
      return perimeter;
    }

  TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge)
  {
     l1 = edge;
     l2 = edge;
     l3 = edge;
  }

  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      l1=Point::Distance(p2, p1);
      l2=Point::Distance(p2, p3);
      l3=Point::Distance(p4, p3);
      l4=Point::Distance(p4, p1);
  }

  void Quadrilateral::AddVertex(const Point &p)
  {
    if (points.size()<4)
        points.push_back(p);
    else
      throw runtime_error ("Il quadrilatero ha già 4 vertici");
  }

  double Quadrilateral::Perimeter() const
  {
      double perimeter = 0;
      perimeter = l1+l2+l3+l4;
      return perimeter;
    }

  Rectangle::Rectangle(const Point &p1, const Point &p2, const Point &p3, const Point &p4): Quadrilateral(p1, p2, p3, p4) {}

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
    l1= base;
    l2= height;
    l3= base;
    l4= height;
  }

  Square::Square(const Point &p1, const Point &p2, const Point &p3, const Point &p4): Rectangle(p1, p2, p3, p4) {}

  Square::Square(const Point &p1, const double &edge)
  {
      l1= edge;
      l2= edge;
      l3= edge;
      l4= edge;
  }

}
