#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

    Point::Point(const double &x,
                 const double &y)
    {
        _x=x;
        _y=y;
    }

    //funzione per calcolare la distanza tra due punti
    double Point::Distance(const Point& X, const Point& Y) {
        return sqrt(pow((X._x-Y._x), 2)+pow((X._y-Y._y), 2));
    }

    Ellipse::Ellipse(const Point &center,
                     const int &a,
                     const int &b)
    {
        _center = center;
        _a = a;
        _b = b;
    }

    double Ellipse::Area() const {
        return M_PI * _a * _b;
    }

    Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius)
    {
    }

    double Circle::Area() const {
        return Ellipse::Area(); //eredita dall'ellisse
    }

    Triangle::Triangle() //costruttore vuoto
    {
        _p1=0;
        _p2=0;
        _p3=0;
    }

    Triangle::Triangle(const Point &p1,
                       const Point &p2,
                       const Point &p3)
    {
        _p1 = p1;
        _p2 = p2;
        _p3 = p3;
        l1=Point::Distance(p2, p1);
        l2=Point::Distance(p2, p3);
        l3=Point::Distance(p1, p3);
        semiP=(l1+l2+l3)*0.5;
    }


    double Triangle::Area() const {
        return sqrt(semiP*(semiP-l1)*(semiP-l2)*(semiP-l3));
    }

    TriangleEquilateral::TriangleEquilateral()
    {
        _p1 = 0;
        _edge = 0;
    }

    TriangleEquilateral::TriangleEquilateral(const Point &p1, const int& edge)
    {
        _p1 = p1;
        l1=edge;
        l2=edge;
        l3=edge;
        semiP=edge*3/2;
    }

    //eredita solo l'area dal triangolo
    double TriangleEquilateral::Area() const {
        return Triangle::Area();
    }

    Quadrilateral::Quadrilateral()
    {
        _p1=0;
        _p2=0;
        _p3=0;
        _p4=0;
    }

    Quadrilateral::Quadrilateral(const Point &p1,
                                 const Point &p2,
                                 const Point &p3,
                                 const Point &p4)
    {
        _p1=p1;
        _p2=p2;
        _p3=p3;
        _p4=p4;
        l1=Point::Distance(p1, p2);
        l2=Point::Distance(p2, p3);
        l3=Point::Distance(p3, p4);
        l4=Point::Distance(p4, p1);
        d=Point::Distance(p1, p3);
        semiP1=(l1+l2+d)*0.5;
        semiP2=(l3+l4+d)*0.5;
    }

    double Quadrilateral::Area() const
    {
        return sqrt(semiP1*(semiP1-l1)*(semiP1-l2)*(semiP1-d)) +sqrt(semiP2*(semiP2-l3)*(semiP2-l4)*(semiP2-d));
    }

    Parallelogram::Parallelogram()
    {
        _p1=0;
        _p2=0;
        _p4=0;
    }

    Parallelogram::Parallelogram(const Point &p1,
                                 const Point &p2,
                                 const Point &p4)
    {
        _p1=p1;
        _p2=p2;
        _p4=p4;
        l1=Point::Distance(p1, p2);
        l2=Point::Distance(p1, p4);
        d=Point::Distance(p2, p4);
        l3=l1;
        l4=l2;
        semiP1=(l1+l2+d)*0.5;
        semiP2=semiP1;
    }

    double Parallelogram::Area() const
    {
        return Quadrilateral::Area();
    }

    Rectangle::Rectangle()
    {
        _base=0;
        _height=0;
    }

    Rectangle::Rectangle(const Point &p1,
                         const int &base,
                         const int &height)
    {
        _base=base;
        _height=height;
        _p1=p1;
        l1=base;
        l2=height;
        l3=base;
        l4=height;
        d=sqrt(l1*l1+l2*l2);
        semiP1=(l1+l2+d)*0.5;
        semiP2=semiP1;
    }

    double Rectangle::Area() const
    {
        return Parallelogram::Area();
    }

    Square::Square()
    {
        _edge=0;
    }

    Square::Square(const Point &p1, const int &edge)
    {
        l1=edge;
        l2=edge;
        l3=edge;
        l4=edge;
        d=l1*sqrt(2);
        semiP1=(l1+l2+d)*0.5;
        semiP2=semiP1;
    }

    double Square::Area() const
    {
        return Rectangle::Area();
    }

}
