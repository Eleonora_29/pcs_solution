 #ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <math.h>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double _x;
      double _y;
      Point(const double& x=0,
            const double& y=0);
      Point(const Point& point) { }

      static double Distance (const Point& X, const Point& Y);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      int _a;
      int _b;
    public:
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle(const Point& center,
             const int& radius);

      double Area() const;
  };


  class Triangle : public IPolygon
  {
    protected:
      Point _p1, _p2, _p3;
      double l1, l2, l3;
      double semiP;
    public:
      Triangle(); //costruttore vuoto
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const;
  };


  class TriangleEquilateral : public Triangle
  {
    protected:
      Point _p1;
      int _edge;
    public:
      TriangleEquilateral();
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      Point _p1, _p2, _p3, _p4;
      double l1, l2, l3, l4, d;
      double semiP1, semiP2;
  public:
      Quadrilateral();
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    protected:
      Point _p1, _p2, _p4;
    public:
      Parallelogram();
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public Parallelogram
  {
    protected:
      Point _p1;
      int _base, _height;
    public:
      Rectangle();
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public Rectangle
  {
    protected:
      Point _p1;
      int _edge;
    public:
      Square();
      Square(const Point& p1,
             const int& edge);

      double Area() const;
  };
}

#endif // SHAPE_H
