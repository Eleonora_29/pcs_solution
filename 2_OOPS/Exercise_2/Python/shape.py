import math

def square(X):
    return X*X

class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y =y

    def distance (X,Y): #definisco una funzione per calcolare la distanza tra due punti
        l = math.sqrt(square(X.x - Y.x) + square(X.y - Y.y))
        return l

class IPolygon:
    def area(self) -> float:
        pass

class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        Point(center.x, center.y)
        self.a = a
        self.b = b

    def area(self) -> float:
        return self.a * self.b * math.pi

class Circle(Ellipse):
    def __init__(self, center:Point, radius: int):
        super().__init__(center, radius, radius) #eredita dall'ellisse

class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        Point(p1.x, p1.y)
        Point(p2.x, p2.y)
        Point(p3.x, p3.y)
        l1= Point.distance(p1, p2)
        l2= Point.distance(p2, p3)
        l3= Point.distance(p3, p1)
        self.l1= l1
        self.l2= l2
        self.l3= l3

    def area(self) -> float:
        semiP = (self.l1 + self.l2 + self.l3) * 0.5
        return math.sqrt(semiP*(semiP-self.l1)*(semiP-self.l2)*(semiP-self.l3))

class TriangleEquilateral(Triangle):
    def __init__(self, p1: Point, edge:int):
        Point(p1.x, p1.y)
        self.l1 = edge
        self.l2 = edge
        self.l3 = edge
        Triangle.area(self) #eredita solo l'area dal triangolo

class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        Point(p1.x, p1.y)
        Point(p2.x, p2.y)
        Point(p3.x, p3.y)
        Point(p4.x, p4.y)
        l1 = Point.distance(p1, p2)
        l2 = Point.distance(p2, p3)
        l3 = Point.distance(p3, p4)
        l4 = Point.distance(p4, p1)
        d = Point.distance(p1, p3)
        self.l1 = l1
        self.l2 = l2
        self.l3 = l3
        self.l4 = l4
        self.d = d

    def area(self) -> float:
        semiP1 = (self.l1 + self.l2 + self.d) * 0.5
        semiP2 = (self.l3 + self.l4 + self.d) * 0.5
        area1 = math.sqrt(semiP1*(semiP1-self.l1)*(semiP1-self.l2)*(semiP1-self.d))
        area2 = math.sqrt(semiP2*(semiP2-self.l3)*(semiP2-self.l4)*(semiP2-self.d))
        return area1 + area2

class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        Point(p1.x, p1.y)
        Point(p2.x, p2.y)
        Point(p4.x, p4.y)
        l1 = Point.distance(p1, p2)
        l2 = Point.distance(p1, p4)
        d = Point.distance(p2, p4)
        self.l1 = l1
        self.l2 = l2
        self.l3 = l1
        self.l4 = l2
        self.d = d
        Quadrilateral.area(self)

class Rectangle(Parallelogram):
    def __init__(self, p1: Point, base: int, height: int):
        Point(p1.x, p1.y)
        self.l1 = base
        self.l2 = height
        self.l3 = base
        self.l4 = height
        d = math.sqrt(square(base)+square(height))
        self.d = d
        Parallelogram.area(self)

class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super().__init__(p1, edge, edge)