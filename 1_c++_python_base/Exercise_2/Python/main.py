import sys
# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text
def importText(inputFilePath):
    file=open(inputFilePath, 'r')
    text=file.readlines()
    file.close
    return True, text
# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    text= str(*text)
    encryptedText="" #inizializzazione stringa
    for i in range(0, len(text)):
        textTemp=ord(text[i]) #prendo il valore numerico corrispondente al carattere
        if (i < len(password)):
            passwordTemp = ord(password[i])
        else:
            passwordTemp=ord(password[i%len(password)])
        encryptedTemp=textTemp+passwordTemp #sommo gli ascii dei caratteri
        if (encryptedTemp>126):
            encryptedTemp=encryptedTemp-94 #tolgo 94 per rimanere nella tabella non estesa
        encryptedText+=chr(encryptedTemp) #prendo i caratteri corrispondenti ai valori numerici, e li concateno
    return True, encryptedText

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(encryptedText, password):
    decryptedText = ""
    for i in range (0, len(encryptedText)):
        encryptedTemp=ord(encryptedText[i])
        if (i < len(password)):
            passwordTemp = ord(password[i])
        else:
            passwordTemp=ord(password[i%len(password)])
        decryptedTemp=encryptedTemp-passwordTemp
        if (decryptedTemp<32):
            decryptedTemp=decryptedTemp+94
        decryptedText+=chr(decryptedTemp)
    return True, decryptedText


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", *text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    text=str(*text)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
